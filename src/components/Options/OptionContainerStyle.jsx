import styled from 'styled-components';

const OptionContainer = styled.div`
  display: flex;
  flex-direction: column;
  ${'' /* justify-content: center; */}
  width: auto;
  height: auto;
  padding-top: 50px;
  color: #551A8B;
  background-color: #ADD8E6;
`;

export default OptionContainer;
