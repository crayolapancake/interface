import styled from 'styled-components';

const Option = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: auto;
  height: 300px;
  color: #551A8B;
  background-color: #98FB98;
`;

export default Option;
