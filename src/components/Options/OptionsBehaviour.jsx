import React, { Component } from 'react';
import OptionContainerStyle from './OptionContainerStyle';
import Option from './Option'

class OptionsBehaviour extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: [""], //null?
    };

    // intead of value, it should be changes to action, reset or fallback and the changable nested elements within these.

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event){
    this.setState({value: event.target.value});
    console.log(this.state)
  }

  handleSubmit(event) {
    alert('Change submitted: ' + this.state.value);
    event.preventDefault()
  }

  render() {
    return (
      <OptionContainerStyle>OCS
        <Option>
        <form onSubmit={this.handleSubmit}>
          Change Fallback Data
          <br />
          <label>
            Change Fallback Statement 1:
            <input
              type="text"
              name="fallbackStatement1"
              value={this.state.value}
              onChange={this.handleChange}
            />
          </label>
          <br />
          <label>
            Change Fallback Statement 2:
            <input
              name="fallbackStatement2"
              value={this.state.value}
              onChange={this.handleChange} />
          </label>
          <br />
          <label>
            Change Fallback Statement 3:
            <input
              name="fallbackStatement3"
              value={this.state.value}
              onChange={this.handleChange} />
          </label>
          <br />
          <input type="submit" value="Submit" />
        </form>
        </Option>
      </OptionContainerStyle>
    )
  }
}

export default OptionsBehaviour;
