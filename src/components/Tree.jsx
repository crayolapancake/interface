
import React, { Component } from 'react';
import treeData from '../data/tree.json';

// one component should take json & put editable questions or whatever into an array
// pass array to another component which will allow the actual editing
// simplify the json so form compoanent only deals with one layer of  questions / answers

class Tree extends Component {

  constructor(props) {
    super(props);

    this.state = {
      statement1: "",
      statement2: "",
      statement3: "",
      treeData: {
        action: [],
        reset: [],
        fallback: [],
      },
    };

    this.standardActions = this.standardActions.bind(this);
    this.resetActions = this.resetActions.bind(this);
    this.fallbackActions = this.fallbackActions.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.setNewData = this.setNewData.bind(this);
  }

  // Called after components mount. Shows state has been set from JSON file.
  componentDidMount() {
    this.setState({ treeData: treeData });
    var fallbackData =  treeData.fallback
    console.log('fallbackData', fallbackData);
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value })
    console.log('s1', this.state.statement1)
    console.log('s2', this.state.statement2)
    console.log('s3', this.state.statement3)
  }

  handleSubmit(event) {
    event.preventDefault();
    console.log("handleSubmit", this.state.newValue )
    this.setNewData()
  }

  setNewData() {
    const submittedFallbackData = [this.state.statement1, this.state.statement2, this.state.statement3]
    console.log('submittedFallbackData', submittedFallbackData)

    //FAILING AT FALLBACK
    this.setState ({ fallback: submittedFallbackData})
    console.log('fallback', this.state.treeData.fallback )
  }

  standardActions(treeData) {
    return this.state.treeData.action;
  }

  resetActions() {
   return this.state.treeData.reset;
  }

  fallbackActions() {
   return this.state.treeData.fallback;
  }

  render() {
    return (
      <div className="tree-components">

        <h2>Fallback Statements</h2>
        {this.fallbackActions().map((item, index) => <p key={index}>{JSON.stringify(item)}</p>)}

        <form
          onSubmit={this.handleSubmit}

          >
          <label>Change Fallback Statement 1: </label>
            <input
              type="type"
              name="statement1"
              defaultValue={this.state.treeData.fallback[0]}
              onChange={this.handleChange}
            />
            <br />
            <label>Change Fallback Statement 2: </label>
            <input
                type="text"
                name="statement2"
                defaultValue={this.state.treeData.fallback[1]}
                onChange={this.handleChange}
            />
            <br />
            <label>Change Fallback Statement 3: </label>
            <input
                type="text"
                name="statement3"
                defaultValue={this.state.treeData.fallback[2]}
                onChange={this.handleChange}
            />
            <br />
            <input type="submit" value="submit" />
            <h4>Changed Fallback Data:</h4>
            <p>{this.state.statement1}</p>
            <p>{this.state.statement2}</p>
            <p>{this.state.statement3}</p>
        </form>
        <hr/>
        <h2>Reset Actions</h2>
        {this.resetActions().map((item, index) => <p key={index}>{JSON.stringify(item)}</p>)}

        <h2>Dig into Reset Array:</h2>
        {this.state.treeData.reset.map((item, index) => {
          return (
            <div key={index}>
              {/* <span>Type: {item.type}</span> */}
              <br />
              {/* dig into action*/}
              <span>Questions: {item.data.question}</span>
              <br/>
              {/* <span>Data.Text: {item.data.answer.text}</span> */}
            </div>
          )
        })}
        <hr/>
        <h2>Standard Chat Actions</h2>
        {/* maps thru standardActions and returns a string of data from JSON */}
        {this.standardActions().map((item, index) => <p key={index}>{JSON.stringify(item)}</p>)}

        <h2>Dig into Chat Actions: </h2>
        {this.state.treeData.action.map((item, index) => {
          return (
            <div key={index}>
              {/* <span>{item.data}</span> */}
            </div>
      )
    })}
      </div>

    );
  }
}

export default Tree;
