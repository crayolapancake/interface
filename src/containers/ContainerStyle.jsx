import styled from 'styled-components';

const ContainerStyle = styled.div`
  display: inline-block;
  justify-content: space-around;
  width: 100%;
  background-color: #DEEFF5
;
  height: auto;
  font-family: "Helveitca";
  font-weight: auto;
  font-size: 24px;
${''/* aligns all text in all child containers */}
  text-align: center;
`;

export default ContainerStyle;
