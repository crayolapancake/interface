import React, { Component } from 'react';
// import OptionsBehaviour from './components/Options/OptionsBehaviour';
import ContainerStyle from './containers/ContainerStyle';
import Header from './components/Header';
import logo from './images/logo.png';
import LogoStyling from './components/LogoStyling';
import Tree from './components/Tree.jsx';
 

class App extends Component {
  constructor(props) {
    super(props);

  }

  render() {
    return (
        <ContainerStyle className="container-style">
          <Header>
            <LogoStyling classame="logo-styling">
              <img
                src={logo}
                alt="Voxsio Logo"
                height="40px"
                width="140px"
              />
            </LogoStyling>
            Voxsio Chat Interface
          </Header>

          <Tree />

        </ContainerStyle>
    );
  }
}

export default App;
